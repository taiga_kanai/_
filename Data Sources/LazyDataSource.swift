//
//  LazyDataSource.swift
//  MarsHabitatPricePredictor
//
//  Created by 金井大河 on 2020/12/18.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct LazyDataSource {
    /// Possible values for greenhouses in the habitat
    let values = [1, 2, 3, 4]
    
    func title(for index: Int) -> String? {
        guard index < values.count else { return nil }
        return String(values[index])
    }
    
    func value(for index: Int) -> Double? {
        guard index < values.count else { return nil }
        return Double(values[index])
    }
}
