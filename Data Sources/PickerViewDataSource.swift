/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Data source for the picker that manages the solar panels, greenhouses and size data sources.
*/

import UIKit

/**
     The common data source for the three features and their picker values. Decouples
     the user interface and the feature specific values.
*/
class PickerDataSource: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - Properties
    
    private let coughDataSource = CoughDataSource()
    private let body_temperatureDataSource = Body_temperatureDataSource()
    private let throatDataSource = ThroatDataSource()
    private let noseDataSource = NoseDataSource()
    private let lazyDataSourse = LazyDataSource()
    
    // MARK: - Helpers
    
    /// Find the title for the given feature.
    func title(for row: Int, feature: Feature) -> String? {
        switch feature {
        case .cough:  return coughDataSource.title(for: row)
        case .body_temperature:  return body_temperatureDataSource.title(for: row)
        case .throat:         return throatDataSource.title(for: row)
        case .nose: return noseDataSource.title(for: row)
        case .lazy: return lazyDataSourse.title(for: row)
        }
    }
    
    /// For the given feature, find the value for the given row.
    func value(for row: Int, feature: Feature) -> Double {
        let value: Double?
        
        switch feature {
        case .cough:      value = coughDataSource.value(for: row)
        case .body_temperature:      value = body_temperatureDataSource.value(for: row)
        case .throat:             value = throatDataSource.value(for: row)
        case .nose:  value = noseDataSource.value(for: row)
        case .lazy:  value = lazyDataSourse.value(for: row)
        }
        
        return value!
    }
    
    // MARK: - UIPickerViewDataSource
    
    /// Hardcoded 3 items in the picker.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 5
    }
    
    /// Find the count of each column of the picker.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch Feature(rawValue: component)! {
        case .cough:  return coughDataSource.values.count
        case .body_temperature:  return body_temperatureDataSource.values.count
        case .throat:         return throatDataSource.values.count
        case .nose:  return noseDataSource.values.count
        case .lazy:  return lazyDataSourse.values.count
        }
    }
}
